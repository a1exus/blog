class Comment < ActiveRecord::Base
	belongs_to :post #, :validate => true
	validates_presence_of :post_id
	validates_presence_of :body
	validates_presence_of :post
#	validates :post, presence: { is: true, message: "does not exist" }
end
